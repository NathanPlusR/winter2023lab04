import java.util.Scanner;

public class Application
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Students[] section3 = new Students[2];
		for (int i = 0; i < section3.length; i++)
		{
			section3[i] = new Students();
			System.out.println("Student number " + (i+1));
			System.out.println("What is this student's name?");
			section3[i].setName(scan.next());
			System.out.println("What is their estimated grade?");
			section3[i].setEstimatedGrade(scan.nextInt());
			System.out.println("What is their happiness level?");
			section3[i].setHappyLevel(scan.nextInt());
			System.out.println("How much have they studied?");
			section3[i].setAmountStudied(scan.nextInt());
			System.out.println();
		}
		
		System.out.println(section3[0].getName());
		System.out.println(section3[0].getEstimatedGrade());
		System.out.println(section3[0].getHappyLevel());
		System.out.println(section3[0].getAmountStudied());
		
		System.out.println(section3[1].getName());
		System.out.println(section3[1].getEstimatedGrade());
		System.out.println(section3[1].getHappyLevel());
		System.out.println(section3[1].getAmountStudied());
	}
}