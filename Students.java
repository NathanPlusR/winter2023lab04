
public class Students
{
	private String name;
	private int estimatedGrade;
	private int happyLevel;
	private int amountLearnt;
	private int amountStudied;
	
	public void study(int hours)
	{
		this.happyLevel -= 2 * hours;
		this.estimatedGrade += 0.5 * hours;
	}
	
	public void socialize(int hours)
	{
		if (hours >= 3)
		{
			this.happyLevel += hours;
		}
		else
		{
			this.happyLevel -= 1;
		}
	}
	
	public void learn(int amountStudied)
	{
		if (amountStudied > 0)
		{
			this.amountLearnt += amountStudied;
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getEstimatedGrade() {
		return this.estimatedGrade;
	}
	
	public int getHappyLevel() {
		return this.happyLevel;
	}
	
	public int getAmountLearnt() {
		return this.amountLearnt;
	}
	
	public int getAmountStudied() {
		return this.amountStudied;
	}
	
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public void setEstimatedGrade(int newEstimatedGrade) {
		this.estimatedGrade = newEstimatedGrade;
	}
	
	public void setHappyLevel(int newHappyLevel) {
		this.happyLevel = newHappyLevel;
	}
	
	public void setAmountStudied(int newAmountStudied) {
		this.amountStudied = newAmountStudied;
	}
}